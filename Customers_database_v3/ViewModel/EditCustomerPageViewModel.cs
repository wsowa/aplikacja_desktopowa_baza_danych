﻿using Customers_database_v3.Model;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Customers_database_v3.ViewModel
{
    class EditCustomerPageViewModel : DependencyObject
    {
        CustomerDbContext db;

        public EditCustomerPageViewModel(Customer newCustomer)
        {
            Customer = newCustomer;

            NewCustomerName = Customer.Name;
            NewCustomerSurname = Customer.Surname;
            NewCustomerDocNo = Customer.Doc_no;
            NewCustomerAddress = Customer.Address;
            NewCustomerOrderNo = Customer.Order_no;
            NewCustomerPrice = Customer.Price;
            NewCustomerGetDate = Convert.ToDateTime(Customer.Get_date).ToString();
            NewCustomerStartDate = Convert.ToDateTime(Customer.Start_date).ToString();
            NewCustomerDescription = Customer.Description;

            SaveCustomerData = new Command(saveCustomerData);
        }
        #region Dependency Property
        public string NewCustomerName
        {
            get { return (string)GetValue(NewCustomerNameProperty); }
            set { SetValue(NewCustomerNameProperty, value); }
        }

        public static readonly DependencyProperty NewCustomerNameProperty =
            DependencyProperty.Register("NewCustomerName", typeof(string), typeof(EditCustomerPageViewModel), new PropertyMetadata(""));


        public string NewCustomerSurname
        {
            get { return (string)GetValue(NewCustomerSurnameProperty); }
            set { SetValue(NewCustomerSurnameProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerSurnameProperty =
            DependencyProperty.Register("NewCustomerSurame", typeof(string), typeof(EditCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerDocNo
        {
            get { return (string)GetValue(NewCustomerDocNoProperty); }
            set { SetValue(NewCustomerDocNoProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerDocNoProperty =
            DependencyProperty.Register("NewCustomerDocNo", typeof(string), typeof(EditCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerAddress
        {
            get { return (string)GetValue(NewCustomerAddressProperty); }
            set { SetValue(NewCustomerAddressProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerAddressProperty =
            DependencyProperty.Register("NewCustomerAddress", typeof(string), typeof(EditCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerOrderNo
        {
            get { return (string)GetValue(NewCustomerOrderNoProperty); }
            set { SetValue(NewCustomerOrderNoProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerOrderNoProperty =
            DependencyProperty.Register("NewCustomerOrderNo", typeof(string), typeof(EditCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerPrice
        {
            get { return (string)GetValue(NewCustomerPriceProperty); }
            set { SetValue(NewCustomerPriceProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerPriceProperty =
            DependencyProperty.Register("NewCustomerPrice", typeof(string), typeof(EditCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerGetDate
        {
            get { return (string)GetValue(NewCustomerGetDateProperty); }
            set { SetValue(NewCustomerGetDateProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerGetDateProperty =
            DependencyProperty.Register("NewCustomerGetDate", typeof(string), typeof(EditCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerStartDate
        {
            get { return (string)GetValue(NewCustomerStartDateProperty); }
            set { SetValue(NewCustomerStartDateProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerStartDateProperty =
            DependencyProperty.Register("NewCustomerStartDate", typeof(string), typeof(EditCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerDescription
        {
            get { return (string)GetValue(NewCustomerDescriptionProperty); }
            set { SetValue(NewCustomerDescriptionProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerDescriptionProperty =
            DependencyProperty.Register("NewCustomerDescription", typeof(string), typeof(EditCustomerPageViewModel), new PropertyMetadata(""));

        public ICommand SaveCustomerData
        {
            get { return (ICommand)GetValue(SaveCustomerDataProperty); }
            set { SetValue(SaveCustomerDataProperty, value); }
        }
        public static readonly DependencyProperty SaveCustomerDataProperty =
            DependencyProperty.Register("SaveCustomerData", typeof(ICommand), typeof(EditCustomerPageViewModel), new PropertyMetadata(null));
        #endregion

        Customer Customer;
        public event Action<bool> OnDataFilled;

        private void saveCustomerData()
        {
            if (OnDataFilled != null)
            {
                if (OnDataFilled != null)
                {
                    if (string.IsNullOrEmpty(NewCustomerName) || string.IsNullOrWhiteSpace(NewCustomerName))
                    {
                        MessageBox.Show("Pole imię nie może być puste!");
                        return;
                    }
                    if (string.IsNullOrEmpty(NewCustomerSurname) || string.IsNullOrWhiteSpace(NewCustomerSurname))
                    {
                        MessageBox.Show("Pole nazwisko nie może być puste!");
                        return;
                    }
                    if (string.IsNullOrEmpty(NewCustomerDocNo) || string.IsNullOrWhiteSpace(NewCustomerDocNo))
                    {
                        MessageBox.Show("Pole nr dowodu nie może być puste!");
                        return;
                    }
                    if (string.IsNullOrEmpty(NewCustomerAddress) || string.IsNullOrWhiteSpace(NewCustomerAddress))
                    {
                        MessageBox.Show("Pole adres nie może być puste!");
                        return;
                    }
                    if (string.IsNullOrEmpty(NewCustomerOrderNo) || string.IsNullOrWhiteSpace(NewCustomerOrderNo))
                    {
                        MessageBox.Show("Pole nr zlecenia nie może być puste!");
                        return;
                    }
                    if (string.IsNullOrEmpty(NewCustomerPrice) || string.IsNullOrWhiteSpace(NewCustomerPrice))
                    {
                        MessageBox.Show("Pole cena nie może być puste!");
                        return;
                    }
                    if (string.IsNullOrEmpty(NewCustomerGetDate.ToString()) || string.IsNullOrWhiteSpace(NewCustomerGetDate.ToString()))
                    {
                        MessageBox.Show("Pole data przyjęcia nie może być puste!");
                        return;
                    }
                    if (string.IsNullOrEmpty(NewCustomerStartDate.ToString()) || string.IsNullOrWhiteSpace(NewCustomerStartDate.ToString()))
                    {
                        MessageBox.Show("Pole data wykonania nie może być puste!");
                        return;
                    }
                    if (string.IsNullOrEmpty(NewCustomerDescription) || string.IsNullOrWhiteSpace(NewCustomerDescription))
                    {
                        MessageBox.Show("Pole zakres prac wykonania nie może być puste!");
                        return;
                    }

                    Customer.Name = NewCustomerName;
                    Customer.Surname = NewCustomerSurname;
                    Customer.Doc_no = NewCustomerDocNo;
                    Customer.Address = NewCustomerAddress;
                    Customer.Order_no = NewCustomerOrderNo;
                    Customer.Price = NewCustomerPrice;
                    Customer.Get_date = Convert.ToDateTime(NewCustomerGetDate);
                    Customer.Start_date = Convert.ToDateTime(NewCustomerStartDate);
                    Customer.Description = NewCustomerDescription;

                    OnDataFilled(true);
                }
            }
        }
    }
}
    
