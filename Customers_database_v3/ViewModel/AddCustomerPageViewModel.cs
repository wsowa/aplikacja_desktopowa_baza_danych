﻿using Customers_database_v3.Model;
using Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Customers_database_v3.ViewModel
{
    class AddCustomerPageViewModel : DependencyObject
    {
        CustomerDbContext db;

        public AddCustomerPageViewModel(Customer customer)
        {
            NewCustomer = customer;
            AddNewCustomer = new Command(addCustomer);
        }

        #region Dependency Property
        public string NewCustomerName
        {
            get { return (string)GetValue(NewCustomerNameProperty); }
            set { SetValue(NewCustomerNameProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerNameProperty =
            DependencyProperty.Register("NewCustomerName", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));


        public string NewCustomerSurname
        {
            get { return (string)GetValue(NewCustomerSurnameProperty); }
            set { SetValue(NewCustomerSurnameProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerSurnameProperty =
            DependencyProperty.Register("NewCustomerSurame", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerDocNo
        {
            get { return (string)GetValue(NewCustomerDocNoProperty); }
            set { SetValue(NewCustomerDocNoProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerDocNoProperty =
            DependencyProperty.Register("NewCustomerDocNo", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerAddress
        {
            get { return (string)GetValue(NewCustomerAddressProperty); }
            set { SetValue(NewCustomerAddressProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerAddressProperty =
            DependencyProperty.Register("NewCustomerAddress", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerOrderNo
        {
            get { return (string)GetValue(NewCustomerOrderNoProperty); }
            set { SetValue(NewCustomerOrderNoProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerOrderNoProperty =
            DependencyProperty.Register("NewCustomerOrderNo", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerPrice
        {
            get { return (string)GetValue(NewCustomerPriceProperty); }
            set { SetValue(NewCustomerPriceProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerPriceProperty =
            DependencyProperty.Register("NewCustomerPrice", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerGetDate
        {
            get { return (string)GetValue(NewCustomerGetDateProperty); }
            set { SetValue(NewCustomerGetDateProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerGetDateProperty =
            DependencyProperty.Register("NewCustomerGetDate", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerStartDate
        {
            get { return (string)GetValue(NewCustomerStartDateProperty); }
            set { SetValue(NewCustomerStartDateProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerStartDateProperty =
            DependencyProperty.Register("NewCustomerStartDate", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerDescription
        {
            get { return (string)GetValue(NewCustomerDescriptionProperty); }
            set { SetValue(NewCustomerDescriptionProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerDescriptionProperty =
            DependencyProperty.Register("NewCustomerDescription", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public ICommand AddNewCustomer
        {
            get { return (ICommand)GetValue(AddNewCustomerProperty); }
            set { SetValue(AddNewCustomerProperty, value); }
        }
        public static readonly DependencyProperty AddNewCustomerProperty =
            DependencyProperty.Register("AddNewCustomer", typeof(ICommand), typeof(AddCustomerPageViewModel), new PropertyMetadata(null));
        #endregion

        private Customer NewCustomer { set; get; }

        public event Action<bool> OnDataFilled;

        private void addCustomer()
        {
            if (OnDataFilled != null)
            {
                if (string.IsNullOrEmpty(NewCustomerName) || string.IsNullOrWhiteSpace(NewCustomerName))
                {
                    MessageBox.Show("Pole imię nie może być puste!");
                    return;
                }
                if (NewCustomerName.Any(c => char.IsDigit(c)))
                {
                    MessageBox.Show("Pole imię nie może zawierac cyfr!");
                    return;
                }

                if (string.IsNullOrEmpty(NewCustomerSurname) || string.IsNullOrWhiteSpace(NewCustomerSurname))
                {
                    MessageBox.Show("Pole nazwisko nie może być puste!");
                    return;
                }
                if (NewCustomerSurname.Any(c => char.IsDigit(c)))
                {
                    MessageBox.Show("Pole nazwisko nie może zawierac cyfr!");
                    return;
                }
                if (string.IsNullOrEmpty(NewCustomerDocNo) || string.IsNullOrWhiteSpace(NewCustomerDocNo))
                {
                    MessageBox.Show("Pole nr dowodu nie może być puste!");
                    return;
                }
                if (string.IsNullOrEmpty(NewCustomerAddress) || string.IsNullOrWhiteSpace(NewCustomerAddress))
                {
                    MessageBox.Show("Pole adres nie może być puste!");
                    return;
                }
                if (string.IsNullOrEmpty(NewCustomerOrderNo) || string.IsNullOrWhiteSpace(NewCustomerOrderNo))
                {
                    MessageBox.Show("Pole nr zlecenia nie może być puste!");
                    return;
                }
                if (NewCustomerOrderNo.Any(c => char.IsLetter(c)))
                {
                    MessageBox.Show("Pole numer zlecenia nie może zawierac liter!");
                    return;
                }
                if (string.IsNullOrEmpty(NewCustomerPrice) || string.IsNullOrWhiteSpace(NewCustomerPrice))
                {
                    MessageBox.Show("Pole cena nie może być puste!");
                    return;
                }
                if (NewCustomerPrice.Any(c => char.IsLetter(c)))
                {
                    MessageBox.Show("Pole cena nie może zawierac liter!");
                    return;
                }
                if (string.IsNullOrEmpty(NewCustomerGetDate.ToString()) || string.IsNullOrWhiteSpace(NewCustomerGetDate.ToString()))
                {
                    MessageBox.Show("Pole data przyjęcia nie może być puste!");
                    return;
                }
                if (string.IsNullOrEmpty(NewCustomerStartDate.ToString()) || string.IsNullOrWhiteSpace(NewCustomerStartDate.ToString()))
                {
                    MessageBox.Show("Pole data wykonania nie może być puste!");
                    return;
                }
                if (string.IsNullOrEmpty(NewCustomerDescription) || string.IsNullOrWhiteSpace(NewCustomerDescription))
                {
                    MessageBox.Show("Pole zakres prac wykonania nie może być puste!");
                    return;
                }

                NewCustomer.Name = NewCustomerName;
                NewCustomer.Surname = NewCustomerSurname;
                NewCustomer.Doc_no = NewCustomerDocNo;
                NewCustomer.Address = NewCustomerAddress;
                NewCustomer.Order_no = NewCustomerOrderNo;
                NewCustomer.Price = NewCustomerPrice;
                NewCustomer.Get_date = Convert.ToDateTime(NewCustomerGetDate);
                NewCustomer.Start_date = Convert.ToDateTime(NewCustomerStartDate);
                NewCustomer.Description = NewCustomerDescription;

                OnDataFilled(true);
                MessageBox.Show("Dodano Klienta!");
               

            }
        }

    }
}

