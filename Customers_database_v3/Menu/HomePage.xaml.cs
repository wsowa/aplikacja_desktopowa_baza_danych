﻿using Customers_database_v3.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Customers_database_v3
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : UserControl, ISwitchable
    {
        public HomePage()
        {
            InitializeComponent();
            
            
        }

        private void Add_button(object sender, RoutedEventArgs e)
        {
            //Switcher.Switch(new AddPage());
        }

        public void UtilizeState(object state)
        {
            throw new NotImplementedException();
        }

        private void ShowCust_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ShowCustomersPage());
        }

        private void Invoice_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new InvoicePage());
        }

        private void Settings_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new SettingsPage());
        }

        private void Authors_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AuthorsPage());
        }

        private void Home_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new HomePage());
        }

        private void Calendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            Calendar calendar = sender as Calendar;

            DateTime? selectedDate = calendar.SelectedDate;

            
        }

        
    }
    }

