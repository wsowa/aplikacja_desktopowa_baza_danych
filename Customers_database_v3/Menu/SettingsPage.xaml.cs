﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Customers_database_v3.Menu
{
    /// <summary>
    /// Interaction logic for SettingsPage.xaml
    /// </summary>
    public partial class SettingsPage : UserControl
    {
        public SettingsPage()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(SettingsPage_Loaded);
        }

        private void Add_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AddPage());
        }

        private void ShowCust_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ShowCustomersPage());
        }

        private void Invoice_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new InvoicePage());
        }

        private void Settings_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new SettingsPage());
        }

        private void Authors_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AuthorsPage());
        }

        private void Home_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new HomePage());
        }
        void SettingsPage_Loaded(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(MenuBar);
            if (null != w)
            {
                w.LocationChanged += delegate (object sender2, EventArgs args)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
                w.SizeChanged += delegate (object sender3, SizeChangedEventArgs e2)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
            }
        }

        

        private void comboCol_Selection(object sender, SelectionChangedEventArgs e)
        {
            Color selectedColor = (Color)(comboCol.SelectedItem as PropertyInfo).GetValue(1, null);
            this.Background = new SolidColorBrush(selectedColor);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            comboCol.ItemsSource = typeof(Colors).GetProperties();
        }
    }
}
