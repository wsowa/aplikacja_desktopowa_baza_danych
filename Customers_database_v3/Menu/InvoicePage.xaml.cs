﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;



namespace Customers_database_v3.Menu
{
    /// <summary>
    /// Interaction logic for InvoicePage.xaml
    /// </summary>
    public partial class InvoicePage : UserControl
    {
        public InvoicePage()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(InvoicePage_Loaded);
        }

        private void Add_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AddPage());
        }

        private void ShowCust_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ShowCustomersPage());
        }

        private void Invoice_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new InvoicePage());
        }

        private void Settings_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new SettingsPage());
        }

        private void Authors_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AuthorsPage());
        }

        private void Home_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new HomePage());
        }

        DataTable MakeDataTable()
        {
            DataTable inv = new DataTable();
            inv.Columns.Add("Imie: ");
            inv.Columns.Add("Nazwisko: ");
            inv.Columns.Add("Adres: ");

            inv.Rows.Add("Adam", "Jakis", "Ulica 12 42-100 Klobuck ");

            return inv;
        }



        void ExportDataTableToPdf(DataTable dtblTable, String strPdfPath, string strHeader)
        {
           


            FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            Paragraph prgHeader = new Paragraph();
            prgHeader.Alignment = Element.ALIGN_CENTER;
            prgHeader.Add(new Chunk("Faktura vat " + Inv_No.Text ));
            prgHeader.SpacingAfter = 10;
            document.Add(prgHeader);

            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_CENTER,1)));
            document.Add(p);
            PdfPTable table = new PdfPTable(3)
            {
                TotalWidth = 800f,
                WidthPercentage = 98
                
            };

            table.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            PdfPCell cell = new PdfPCell();

            cell.Colspan = 3;
            
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
           
            table.AddCell("Nabywca: "
                           + "\n"  +Inv_Cust_Nazwa.Text 
                           + "\n"  + Inv_Cust_Imie.Text
                           + "\n" + Inv_Cust_Nazwisko.Text
                           + "\n" + Inv_Cust_Adres.Text
                           + "\nNIP: " + Inv_Cust_NIP.Text
                          );

            table.AddCell("Sprzedawca: "
                           + "\n"  +Inv_Our_Nazwa.Text
                           + "\n" + Inv_Our_Imie.Text
                           + "\n" + Inv_Our_Nazwisko.Text
                           + "\n" + Inv_Our_Adres.Text
                           + "\nNIP: " + Inv_Our_NIP.Text
                          );

            table.AddCell("Data wystawienia: " + DateTime.Now.ToShortDateString()
                           + "\nData platnosci: " + (DateTime.Now.AddDays(14).ToShortDateString()) );


            table.SpacingAfter = 20;
            table.SpacingBefore = 20;
            document.Add(table);
            document.Add(p);

            double vat = 0;
            double vatDoubleType= 1;
            string vatValue = vatBox.SelectionBoxItem.ToString();
            if (vatValue == "0%")
            {
                
                vat = 0;
            }
            if (vatValue == "5%")
            {
                vatValue = "5";
                vatDoubleType = double.Parse(vatValue);
                vatDoubleType = vatDoubleType / 100;
                vat = double.Parse(Inv_Cust_Cena.Text);
                vat = vat * vatDoubleType;
                
            }

            if (vatValue == "8%")
            {
                vatValue = "8";
                vatDoubleType = double.Parse(vatValue);
                vatDoubleType = vatDoubleType / 100;
                vat = double.Parse(Inv_Cust_Cena.Text);
                vat = vat * vatDoubleType;
            }

            if (vatValue == "23%")
            {
                vatValue = "23";
                vatDoubleType = double.Parse(vatValue);
                vatDoubleType = vatDoubleType / 100;
                vat = double.Parse(Inv_Cust_Cena.Text);
                vat = vat * vatDoubleType;
            }
            double cena = 0;
            double cenaBezRabatu = 0;
            string rab = "";
            string rabatValue = rabatBox.SelectionBoxItem.ToString();
            if (rabatValue == "0%")
            {

                
                cenaBezRabatu = double.Parse(Inv_Cust_Cena.Text);
                cenaBezRabatu = cenaBezRabatu + vat;
                cena = cenaBezRabatu;
            }

            if (rabatValue == "2%")
            {

                
                cena = double.Parse(Inv_Cust_Cena.Text);
                cenaBezRabatu = cena + vat;
                cena = cenaBezRabatu * 0.98;
                rab = " z rabatem 2%";
            }

            if (rabatValue == "5%")
            {


                cena = double.Parse(Inv_Cust_Cena.Text);
                cenaBezRabatu = cena + vat;
                cena = cenaBezRabatu * 0.95;
                rab = " z rabatem 5%";
            }
            if (rabatValue == "8%")
            {


                cena = double.Parse(Inv_Cust_Cena.Text);
                cenaBezRabatu = cena + vat;
                cena = cenaBezRabatu * 0.92;
                rab = " z rabatem 8%";
            }
            if (rabatValue == "10%")
            {


                cena = double.Parse(Inv_Cust_Cena.Text);
                cenaBezRabatu = cena + vat;
                cena = cenaBezRabatu * 0.90;
                rab = " z rabatem 10%";
            }

            PdfPTable table2 = new PdfPTable(6)
            {
                TotalWidth = 800f,
                WidthPercentage = 98

            };

            PdfPCell cell2 = new PdfPCell();

            cell.Colspan = 3;

            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right

            table2.AddCell("#");
            table2.AddCell("Nazwa");
            table2.AddCell("Cena netto");
            table2.AddCell("Stawka VAT");
            table2.AddCell("Kwota VAT");
            table2.AddCell("Wartosc brutto");
            table2.AddCell("1");
            table2.AddCell(Inv_Cust_Opis.Text);
            table2.AddCell(Inv_Cust_Cena.Text);
            table2.AddCell(vatBox.SelectionBoxItem.ToString());

            
            

            table2.AddCell(vat.ToString("0.######"));
            
            //cena = double.Parse(Inv_Cust_Cena.Text);
            //cena = cena + vat;

            table2.AddCell(cenaBezRabatu.ToString("0.######"));
            table2.SpacingBefore = 20;
            table2.SpacingAfter = 20;
            document.Add(table2);

            Paragraph prgPrice = new Paragraph();
            prgPrice.Alignment = Element.ALIGN_LEFT;
            prgPrice.Add(new Chunk("Razem do zaplaty: " + cena + " zl" + rab));
            prgPrice.SpacingAfter = 40;
            document.Add(prgPrice);


            PdfPTable table3 = new PdfPTable(3)
            {
                TotalWidth = 800f,
                WidthPercentage = 98

            };
            table3.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            PdfPCell cell3 = new PdfPCell();

            cell.Colspan = 3;
            
            cell.HorizontalAlignment = 1;

            table3.AddCell("................" + "\nPodpis nabywcy");
            table3.AddCell("");
            table3.AddCell("................" + "\nPodpis sprzedawcy");
            document.Add(table3);
            //Paragraph prgSign = new Paragraph();
            //prgSign.Alignment = Element.ALIGN_LEFT;
            //prgSign.Add(new Chunk("................"));
            //prgSign.Add(new Chunk("\nPodpis nabywcy"));
            //document.Add(prgSign);

            //Paragraph prgSign2 = new Paragraph();
            //prgSign2.Alignment = Element.ALIGN_RIGHT;
            //prgSign2.Add(new Chunk("................"));
            //prgSign2.Add(new Chunk("\nPodpis sprzedawcy"));
            //document.Add(prgSign2);

            //Paragraph prgCust = new Paragraph();
            //prgCust.Alignment = Element.ALIGN_LEFT;
            //prgCust.Add(new Chunk(Inv_Cust_Nazwa.Text));
            //prgCust.Add(new Chunk("\n" + Inv_Cust_Imie.Text));
            //prgCust.Add(new Chunk("\n" + Inv_Cust_Nazwisko.Text));
            //prgCust.Add(new Chunk("\n" + Inv_Cust_Adres.Text));
            //prgCust.Add(new Chunk("\n" + Inv_Cust_NIP.Text));
            //document.Add(prgCust);

            //Paragraph prgOur = new Paragraph();
            //prgOur.Alignment = Element.ALIGN_CENTER;
            //prgOur.Add(new Chunk(Inv_Our_Nazwa.Text));
            //prgOur.Add(new Chunk("\n" + Inv_Our_Imie.Text));
            //prgOur.Add(new Chunk("\n" + Inv_Our_Nazwisko.Text));
            //prgOur.Add(new Chunk("\n" + Inv_Our_Adres.Text));
            //prgOur.Add(new Chunk("\n" + Inv_Our_NIP.Text));
            //document.Add(prgOur);

            //Paragraph prgData = new Paragraph();
            //prgData.Alignment = Element.ALIGN_RIGHT;
            //prgData.Add(new Chunk("Data wystawienia: " + DateTime.Now.ToShortDateString()));
            //prgData.Add(new Chunk("\nData platnosci: " + (DateTime.Now.AddDays(14).ToShortDateString())));
            //prgData.Add(new Chunk("\nNumer faktury: " + Inv_No.Text ));

            //document.Add( prgData);









            //Drukowanie
            //PrintDialog printDialog = new PrintDialog();
            //printDialog.PageRangeSelection = PageRangeSelection.AllPages;
            //printDialog.UserPageRangeEnabled = true;
            //bool? doPrint = printDialog.ShowDialog();
            //if (doPrint != true)
            //{
            //    return;
            //}


            document.Close();
            writer.Close();
            fs.Close();
        }

        private void ButtonPrintPressed(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(Inv_Cust_Nazwa.Text) || string.IsNullOrWhiteSpace(Inv_Cust_Nazwa.Text))
            {
                MessageBox.Show("Pole nazwa klienta nie może być puste!");
                return;
            }
            if (Inv_Cust_Nazwa.Text.Any(c => char.IsDigit(c)))
            {
                MessageBox.Show("Pole nazwa klienta nie może zawierac cyfr!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_Cust_Imie.Text) || string.IsNullOrWhiteSpace(Inv_Cust_Imie.Text))
            {
                MessageBox.Show("Pole imię klienta nie może być puste!");
                return;
            }
            if (Inv_Cust_Imie.Text.Any(c => char.IsDigit(c)))
            {
                MessageBox.Show("Pole imię klienta nie może zawierac cyfr!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_Cust_Nazwisko.Text) || string.IsNullOrWhiteSpace(Inv_Cust_Nazwisko.Text))
            {
                MessageBox.Show("Pole nazwisko klienta nie może być puste!");
                return;
            }
            if (Inv_Cust_Nazwisko.Text.Any(c => char.IsDigit(c)))
            {
                MessageBox.Show("Pole nazwisko klienta nie może zawierac cyfr!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_Cust_Adres.Text) || string.IsNullOrWhiteSpace(Inv_Cust_Adres.Text))
            {
                MessageBox.Show("Pole adres klienta nie może być puste!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_Cust_NIP.Text) || string.IsNullOrWhiteSpace(Inv_Cust_NIP.Text))
            {
                MessageBox.Show("Pole NIP klienta nie może być puste!");
                return;
            }
            if (Inv_Cust_NIP.Text.Any(c => char.IsLetter(c)))
            {
                MessageBox.Show("Pole NIP klienta nie może zawierac liter!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_Cust_Opis.Text) || string.IsNullOrWhiteSpace(Inv_Cust_Opis.Text))
            {
                MessageBox.Show("Pole usługa klienta nie może być puste!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_Cust_Cena.Text) || string.IsNullOrWhiteSpace(Inv_Cust_Cena.Text))
            {
                MessageBox.Show("Pole cena netto nie może być puste!");
                return;
            }
            if (Inv_Cust_Cena.Text.Any(c => char.IsLetter(c)))
            {
                MessageBox.Show("Pole cena netto nie może zawierac liter!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_Our_Nazwa.Text) || string.IsNullOrWhiteSpace(Inv_Our_Nazwa.Text))
            {
                MessageBox.Show("Pole nazwa nie może być puste!");
                return;
            }
            if (Inv_Our_Nazwa.Text.Any(c => char.IsDigit(c)))
            {
                MessageBox.Show("Pole nazwa nie może zawierac cyfr!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_Our_Imie.Text) || string.IsNullOrWhiteSpace(Inv_Our_Imie.Text))
            {
                MessageBox.Show("Pole imie nie może być puste!");
                return;
            }
            if (Inv_Our_Imie.Text.Any(c => char.IsDigit(c)))
            {
                MessageBox.Show("Pole imie nie może zawierac cyfr!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_Our_Nazwisko.Text) || string.IsNullOrWhiteSpace(Inv_Our_Nazwisko.Text))
            {
                MessageBox.Show("Pole nazwisko nie może być puste!");
                return;
            }
            if (Inv_Our_Nazwisko.Text.Any(c => char.IsDigit(c)))
            {
                MessageBox.Show("Pole nazwisko nie może zawierac cyfr!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_Our_Adres.Text) || string.IsNullOrWhiteSpace(Inv_Our_Adres.Text))
            {
                MessageBox.Show("Pole adres nie może być puste!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_Our_NIP.Text) || string.IsNullOrWhiteSpace(Inv_Our_NIP.Text))
            {
                MessageBox.Show("Pole NIP nie może być puste!");
                return;
            }
            if (Inv_Our_NIP.Text.Any(c => char.IsLetter(c)))
            {
                MessageBox.Show("Pole NIP nie może zawierac liter!");
                return;
            }
            if (string.IsNullOrEmpty(Inv_No.Text) || string.IsNullOrWhiteSpace(Inv_No.Text))
            {
                MessageBox.Show("Pole numer faktury nie może być puste!");
                return;
            }
            if (Inv_No.Text.Any(c => char.IsLetter(c)))
            {
                MessageBox.Show("Pole numer faktury nie może zawierac liter!");
                return;
            }
            DataTable dtbl = MakeDataTable();
            var openFileDialog = new Microsoft.Win32.SaveFileDialog
            {
                DefaultExt = ".pdf",
                Filter = "PDF files (.pdf)|.pdf|All files (.)|*.*"
            };
            var fileOpenResult = openFileDialog.ShowDialog();
            ExportDataTableToPdf(dtbl, openFileDialog.FileName, "Faktura");
            if (fileOpenResult != true)
            {
                return;
            }
        }

        private void ButtonPrinterPressed(object sender, RoutedEventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            printDialog.PageRangeSelection = PageRangeSelection.AllPages;
            printDialog.UserPageRangeEnabled = true;
            bool? doPrint = printDialog.ShowDialog();
            if (doPrint != true)
            {
                return;
            }
        }
        void InvoicePage_Loaded(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(MenuBar);
            if (null != w)
            {
                w.LocationChanged += delegate (object sender2, EventArgs args)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
                w.SizeChanged += delegate (object sender3, SizeChangedEventArgs e2)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
            }
        }

        private void InvCustList_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ShowCustList());
        }
    }
}
