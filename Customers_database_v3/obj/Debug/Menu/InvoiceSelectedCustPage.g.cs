﻿#pragma checksum "..\..\..\Menu\InvoiceSelectedCustPage.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3B3A1D79C0B038C98E5CF7E2FA927812745F496B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Customers_database_v3.Menu;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Customers_database_v3.Menu {
    
    
    /// <summary>
    /// InvoiceSelectedCustPage
    /// </summary>
    public partial class InvoiceSelectedCustPage : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 41 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Cust_Nazwa;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Cust_Imie;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Cust_Nazwisko;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Cust_Adres;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Cust_NIP;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Cust_Opis;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Cust_Cena;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Our_Nazwa;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Our_Imie;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Our_Nazwisko;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Our_Adres;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_Our_NIP;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Inv_No;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox rabatBox;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem Rzero;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem Rdwa;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem Rpiec;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem Rosiem;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem Rdziesiec;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox vatBox;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem zero;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem piec;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem osiem;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem dwatrzy;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonPrint;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonPrinter;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton toggleMenu;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup MenuBar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Customers_database_v3;component/menu/invoiceselectedcustpage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Inv_Cust_Nazwa = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.Inv_Cust_Imie = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.Inv_Cust_Nazwisko = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.Inv_Cust_Adres = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.Inv_Cust_NIP = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.Inv_Cust_Opis = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.Inv_Cust_Cena = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.Inv_Our_Nazwa = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.Inv_Our_Imie = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.Inv_Our_Nazwisko = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.Inv_Our_Adres = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.Inv_Our_NIP = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.Inv_No = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.rabatBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 15:
            this.Rzero = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 16:
            this.Rdwa = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 17:
            this.Rpiec = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 18:
            this.Rosiem = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 19:
            this.Rdziesiec = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 20:
            this.vatBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 21:
            this.zero = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 22:
            this.piec = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 23:
            this.osiem = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 24:
            this.dwatrzy = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 25:
            
            #line 101 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.ButtonBackPressed);
            
            #line default
            #line hidden
            return;
            case 26:
            this.buttonPrint = ((System.Windows.Controls.Button)(target));
            
            #line 105 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
            this.buttonPrint.Click += new System.Windows.RoutedEventHandler(this.ButtonPrintPressed);
            
            #line default
            #line hidden
            return;
            case 27:
            this.ButtonPrinter = ((System.Windows.Controls.Button)(target));
            
            #line 110 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
            this.ButtonPrinter.Click += new System.Windows.RoutedEventHandler(this.ButtonPrinterPressed);
            
            #line default
            #line hidden
            return;
            case 28:
            this.toggleMenu = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            return;
            case 29:
            this.MenuBar = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            case 30:
            
            #line 115 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Home_button);
            
            #line default
            #line hidden
            return;
            case 31:
            
            #line 119 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Add_button);
            
            #line default
            #line hidden
            return;
            case 32:
            
            #line 126 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.ShowCust_button);
            
            #line default
            #line hidden
            return;
            case 33:
            
            #line 133 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Invoice_button);
            
            #line default
            #line hidden
            return;
            case 34:
            
            #line 140 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Settings_button);
            
            #line default
            #line hidden
            return;
            case 35:
            
            #line 147 "..\..\..\Menu\InvoiceSelectedCustPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Authors_button);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

