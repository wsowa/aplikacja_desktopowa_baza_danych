﻿#pragma checksum "..\..\..\Menu\EditPage.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7A43D9CCEEAD756B35FB0DB2B48F9F3A5625D8D0"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Customers_database_v3.Menu;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Customers_database_v3.Menu {
    
    
    /// <summary>
    /// EditPage
    /// </summary>
    public partial class EditPage : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 35 "..\..\..\Menu\EditPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox imie;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\Menu\EditPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox nazwisko;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\Menu\EditPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox nr_dowodu;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\Menu\EditPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox adres;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Menu\EditPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox nr_zlecenia;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Menu\EditPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox cena;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\Menu\EditPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox data_przyj;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\Menu\EditPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox data_wyk;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Menu\EditPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox zakr_prac;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\Menu\EditPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton toggleMenu;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\Menu\EditPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup MenuBar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Customers_database_v3;component/menu/editpage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Menu\EditPage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.imie = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.nazwisko = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.nr_dowodu = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.adres = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.nr_zlecenia = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.cena = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.data_przyj = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.data_wyk = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.zakr_prac = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            
            #line 65 "..\..\..\Menu\EditPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.ShowCust_button);
            
            #line default
            #line hidden
            return;
            case 11:
            this.toggleMenu = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            return;
            case 12:
            this.MenuBar = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            case 13:
            
            #line 71 "..\..\..\Menu\EditPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Home_button);
            
            #line default
            #line hidden
            return;
            case 14:
            
            #line 75 "..\..\..\Menu\EditPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Add_button);
            
            #line default
            #line hidden
            return;
            case 15:
            
            #line 82 "..\..\..\Menu\EditPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.ShowCust_button);
            
            #line default
            #line hidden
            return;
            case 16:
            
            #line 89 "..\..\..\Menu\EditPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Invoice_button);
            
            #line default
            #line hidden
            return;
            case 17:
            
            #line 96 "..\..\..\Menu\EditPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Settings_button);
            
            #line default
            #line hidden
            return;
            case 18:
            
            #line 103 "..\..\..\Menu\EditPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Authors_button);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

