﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    
    public class Customer
    {
        
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Doc_no { get; set; }
        public string Address { get; set; }
        public string Order_no { get; set; }
        public string Price { get; set; }
        public DateTime Get_date { get; set; }
        public DateTime Start_date { get; set; }
        public string Description { get; set; }
    }
}
