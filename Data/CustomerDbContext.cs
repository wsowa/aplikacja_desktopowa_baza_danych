﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{

    public class CustomerDbContext : DbContext
    {
        private static CustomerDbContext _context;
        public CustomerDbContext() : base("CustomersDB") { }

        public static CustomerDbContext GetContext()
        {
            if (_context == null)
            {
                _context = new CustomerDbContext();
            }
            return _context;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>();
            

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Customer> Customers { get; set; }
       
    }
}
